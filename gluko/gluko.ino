#include <Wire.h>
#include <SoftwareSerial.h>


SoftwareSerial btSerial(4, 3); // 4 RX, 3 TX.
float threshold = 3.25;
int btCode;
float previous_voltage;
float current_voltage;

int STRIP_DETECT = 2;
int VCC_LEDS = 5;
int LED_BATERIA_BAJA = 7;
int LED_CALCULANDO = 8;
int LED_ENCENDIDO = 9;

void setup() {
  Serial.begin(9600);
  pinMode(STRIP_DETECT, INPUT);
  pinMode(LED_ENCENDIDO, OUTPUT);
  pinMode(LED_BATERIA_BAJA, OUTPUT);
  pinMode(LED_CALCULANDO, OUTPUT);
  pinMode(VCC_LEDS, OUTPUT);

  btSerial.begin(9600);
}

void loop() {

  digitalWrite(LED_ENCENDIDO, LOW);
  digitalWrite(LED_CALCULANDO, HIGH);
  digitalWrite(VCC_LEDS, HIGH);
  checkBatteryLevel();

  previous_voltage = analogRead(0) * (5.0 / 1023.0);

  if (btSerial.available() > 0) {
    
    checkBatteryLevel();
    btCode = btSerial.read();
 
    if (btCode == 1 || btCode == 49) {
      btSerial.flush();
      while(1){
        if(digitalRead(STRIP_DETECT) == 1){
          break;
        }
      }
      while(1){
        float current_voltage = analogRead(0) * (5.0 / 1023.0);
        //Serial.println(current_voltage,2);
        if((current_voltage) > threshold) {
          break;
        } else {
          previous_voltage = current_voltage;
        }
      }
      digitalWrite(LED_CALCULANDO, LOW);
      delay(5000);
      current_voltage = analogRead(0) * (5.0 / 1023.0);
       btSerial.println(455*current_voltage-1453,2);
      //btSerial.println(455*(current_voltage - previous_voltage)-20.5,2);
      //btSerial.flush();
      digitalWrite(LED_CALCULANDO, HIGH);
    }

  }
}

void checkBatteryLevel() {
  if (analogRead(2)* (5.0 / 1023.0) < 3.8) {
      digitalWrite(LED_BATERIA_BAJA, LOW);
    } else {
      digitalWrite(LED_BATERIA_BAJA, HIGH);
    }
}
